import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  user : User;
  constructor(
    private userService: UserService,
    private route: ActivatedRoute){ }

  ngOnInit() {
    const userId = +this.route.snapshot.paramMap.get('id');
    this.userService.getUserById(userId).subscribe(user =>{
      this.user = user;
    })
  }
}
