import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { UserAddComponent } from './user-add/user-add.component';


const routes: Routes = [
  { path : '', component: UserListComponent},
  { path : 'details/:id', component: UserDetailsComponent},
  { path : 'update/:id', component: UserUpdateComponent},
  { path : 'add', component: UserAddComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
