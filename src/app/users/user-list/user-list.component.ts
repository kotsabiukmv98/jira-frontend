import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];
  constructor(private userService: UserService){
  }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });
  }

  delete(userId: number){
    this.userService.deleteUser(userId).subscribe(message =>{
      console.log(`Deleted team with ID ${userId} ` + message);
      this.refresh();
    });
  }
}
