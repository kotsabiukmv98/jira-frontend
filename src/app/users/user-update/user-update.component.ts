import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NewUser } from 'src/app/shared/models/newUser';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  userForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    email: new FormControl(),
    teamId: new FormControl(),
    birthday: new FormControl()
  });

  private userId: number;
  
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('id');

    this.userService.getUserById(this.userId)
      .subscribe(user => {
        this.userForm.patchValue({
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          teamId: user.teamId,
          birthday: user.birthday
        });
      }); 
  }
  save(){
    const newUser: NewUser = {
      firstName: this.userForm.value.firstName,
      lastName: this.userForm.value.lastName,
      email: this.userForm.value.email,
      teamId: this.userForm.value.teamId,
      birthday: this.userForm.value.birthday,
    };
    this.userService.updateUser(this.userId, newUser).subscribe(() => {
        this.router.navigateByUrl('/users'); 
      });
  }

}
