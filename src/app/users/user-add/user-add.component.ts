import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { NewUser } from 'src/app/shared/models/newUser';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
  
  userForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    email: new FormControl(),
    teamId: new FormControl(),
    birthday: new FormControl()
  });

  constructor(
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
  }
  save(){
    const newUser: NewUser = {
      firstName: this.userForm.value.firstName,
      lastName: this.userForm.value.firstName,
      email: this.userForm.value.email,
      teamId: this.userForm.value.teamId,
      birthday: this.userForm.value.birthday,
    };
    this.userService.addUser(newUser).subscribe(() => {
      debugger;
        this.router.navigateByUrl('/users'); 
      });
  }
}
