import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/shared/models/team';
import { TeamService } from 'src/app/shared/services/team.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {
  team : Team;
  constructor(
    private teamService: TeamService,
    private route: ActivatedRoute){ }

  ngOnInit() {
    const teamId = +this.route.snapshot.paramMap.get('id');
    this.teamService.getTeamById(teamId).subscribe(team =>{
      this.team = team;
    });
  }
}
