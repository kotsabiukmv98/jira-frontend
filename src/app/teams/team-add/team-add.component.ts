import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/shared/services/team.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Team } from 'src/app/shared/models/team';
import { Router } from '@angular/router';
import { NewTeam } from 'src/app/shared/models/newTeam';

@Component({
  selector: 'app-team-add',
  templateUrl: './team-add.component.html',
  styleUrls: ['./team-add.component.css']
})
export class TeamAddComponent implements OnInit {

  teamForm = new FormGroup({
    name: new FormControl()
  });

  constructor(
    private teamService: TeamService,
    private router: Router) { }

  ngOnInit() {
  }

  save(){
    const newTeam: NewTeam = {
      name: this.teamForm.value.name
    };
    this.teamService.addTeam(newTeam).subscribe(() => {
        this.router.navigateByUrl('/teams'); 
      });
  }
}
