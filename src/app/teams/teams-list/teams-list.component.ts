import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/shared/services/team.service';
import { Team } from 'src/app/shared/models/team';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {

  teams: Team[];
  constructor(private teamService: TeamService) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.teamService.getTeams().subscribe(teams => {
      this.teams = teams;
    });
  }


  delete(teamId: number){
    this.teamService.deleteTeam(teamId).subscribe(() =>{
      console.log(`Deleted team with ID ${teamId}`);
      this.refresh();
    });
  }
}
