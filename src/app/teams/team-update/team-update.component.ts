import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamService } from 'src/app/shared/services/team.service';
import { FormGroup, FormControl } from '@angular/forms';
import { NewTeam } from 'src/app/shared/models/newTeam';

@Component({
  selector: 'app-team-update',
  templateUrl: './team-update.component.html',
  styleUrls: ['./team-update.component.css']
})
export class TeamUpdateComponent implements OnInit {
  
  teamForm = new FormGroup({
    name: new FormControl()
  })

  private teamId: number;

  constructor(
    private route: ActivatedRoute,
    private teamService: TeamService,
    private router: Router) { }

  ngOnInit() {
    this.teamId = +this.route.snapshot.paramMap.get('id');
    
    this.teamService.getTeamById(this.teamId)
      .subscribe(team =>{
        this.teamForm.patchValue({
          name: team.name
        });
      });    
  }

  save(){
    const newTeam: NewTeam = {
      name: this.teamForm.value.name
    };
    
    this.teamService.updateTeam(this.teamId, newTeam)
      .subscribe((() => {
        this.router.navigateByUrl('/teams');
      }));
  }
}
