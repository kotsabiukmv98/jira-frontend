import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { TeamUpdateComponent } from './team-update/team-update.component';
import { TeamAddComponent } from './team-add/team-add.component';


const routes: Routes = [
  { path : '', component: TeamsListComponent},
  { path : 'details/:id', component: TeamDetailsComponent},
  { path : 'update/:id', component: TeamUpdateComponent},
  { path : 'add', component: TeamAddComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsRoutingModule { }
