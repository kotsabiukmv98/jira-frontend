import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamsRoutingModule } from './teams-routing.module';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamUpdateComponent } from './team-update/team-update.component';
import { TeamAddComponent } from './team-add/team-add.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TeamsListComponent,
    TeamDetailsComponent,
    TeamUpdateComponent,
    TeamAddComponent
  ],
  imports: [
    CommonModule,
    TeamsRoutingModule,
    ReactiveFormsModule
  ]
})
export class TeamsModule { }
