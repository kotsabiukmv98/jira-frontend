import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path : '', component: WelcomeComponent},
  { path : 'home', component: WelcomeComponent},
  { path : 'teams', loadChildren: './teams/teams.module#TeamsModule'},
  { path : 'users', loadChildren: './users/users.module#UsersModule'},
  { path : 'projects', loadChildren: './projects/projects.module#ProjectsModule'},
  { path : 'tasks', loadChildren: './tasks/tasks.module#TasksModule'},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
