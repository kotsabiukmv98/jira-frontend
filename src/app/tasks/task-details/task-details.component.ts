import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/shared/models/task';
import { TaskService } from 'src/app/shared/services/task.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {

  task: Task;
  constructor(private teamService: TaskService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const taskId = +this.route.snapshot.paramMap.get('id');
    this.teamService.getTaskById(taskId).subscribe(task =>{
      this.task = task;
    });
  }

}
