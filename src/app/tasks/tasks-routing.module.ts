import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskUpdateComponent } from './task-update/task-update.component';
import { TaskAddComponent } from './task-add/task-add.component';


const routes: Routes = [
  { path : '', component: TasksListComponent},
  { path : 'details/:id', component: TaskDetailsComponent},
  { path : 'update/:id', component: TaskUpdateComponent},
  { path : 'add', component: TaskAddComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
