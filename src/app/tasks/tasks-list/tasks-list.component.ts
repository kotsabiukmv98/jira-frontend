import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/shared/services/task.service';
import { Task } from 'src/app/shared/models/task';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

  tasks: Task[];
  constructor(private taskService: TaskService){
  }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.taskService.getTasks().subscribe(tasks => {
      this.tasks = tasks;
    })
  }
  delete(taskId: number){
    this.taskService.deleteTask(taskId).subscribe(() =>{
      console.log(`Deleted task with ID ${taskId}`);
      this.refresh();
    })
  }
}
