import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TaskService } from 'src/app/shared/services/task.service';
import { FormGroup, FormControl } from '@angular/forms';
import { NewTask } from 'src/app/shared/models/newTask';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {
  
  taskForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    state: new FormControl(),
    projectId: new FormControl(),
    performerId: new FormControl()
  });

  constructor(
    private teamService: TaskService,
    private router: Router){ }

  ngOnInit(){
  }

  save(){
    const newTask: NewTask = {
      name: this.taskForm.value.name,
      description: this.taskForm.value.description,
      state: this.taskForm.value.state,
      projectId: this.taskForm.value.projectId,
      performerId: this.taskForm.value.performerId
    };
    console.log(newTask);
    
    this.teamService.addTask(newTask).subscribe(task => {
      console.log(task);
      this.router.navigateByUrl('/tasks');
    })
  }
}
