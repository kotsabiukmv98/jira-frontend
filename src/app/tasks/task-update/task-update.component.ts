import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TaskService } from 'src/app/shared/services/task.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NewTask } from 'src/app/shared/models/newTask';

@Component({
  selector: 'app-task-update',
  templateUrl: './task-update.component.html',
  styleUrls: ['./task-update.component.css']
})
export class TaskUpdateComponent implements OnInit {

  taskForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    state: new FormControl(),
    projectId: new FormControl(),
    performerId: new FormControl()
  });
  
  private taskId: number;

  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private router: Router){ }

  ngOnInit(){
    this.taskId = +this.route.snapshot.paramMap.get('id');
    this.taskService.getTaskById(this.taskId)
    .subscribe(task =>{
      this.taskForm.patchValue({
        name: task.name,
        description: task.description,
        state: task.state,
        projectId: task.projectId,
        performerId: task.performerId
      });
    }); 
  }

  save(){
    const newTask: NewTask = {
      name: this.taskForm.value.name,
      description: this.taskForm.value.description,
      state: this.taskForm.value.state,
      projectId: this.taskForm.value.projectId,
      performerId: this.taskForm.value.performerId
    };
    debugger;
    this.taskService.updateTask(this.taskId, newTask).subscribe(() => {
      this.router.navigateByUrl('/tasks');
    })
  }
}
