import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Project } from 'src/app/shared/models/project';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  project: Project;
  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const projectId = +this.route.snapshot.paramMap.get('id');
    this.projectService.getProjectById(projectId).subscribe(project =>{
      this.project = project;
    });
  }
}
