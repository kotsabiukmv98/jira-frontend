import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NewProject } from 'src/app/shared/models/newProject';

@Component({
  selector: 'app-project-update',
  templateUrl: './project-update.component.html',
  styleUrls: ['./project-update.component.css']
})
export class ProjectUpdateComponent implements OnInit {

  projectForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    deadline: new FormControl(),
    authorId: new FormControl(),
    teamId: new FormControl()
  });

  private projectId: number;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router) { }

  ngOnInit() {
    this.projectId = +this.route.snapshot.paramMap.get('id');
    
    this.projectService.getProjectById(this.projectId)
      .subscribe(project => {
        this.projectForm.patchValue({
          name: project.name,
          description: project.description,
          deadline: project.deadline,
          authorId: project.authorId,
          teamId: project.teamId
        });
      });
  }
  save(){
    const newProject: NewProject = {
      name: this.projectForm.value.name,
      description: this.projectForm.value.description,
      deadline: this.projectForm.value.deadline,
      authorId: this.projectForm.value.authorId,
      teamId: this.projectForm.value.teamId
    };
    this.projectService.updateProject(this.projectId, newProject).subscribe(() => {
        this.router.navigateByUrl('/projects'); 
      });
  }
}
