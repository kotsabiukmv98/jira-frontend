import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectUpdateComponent } from './project-update/project-update.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';


const routes: Routes = [
  { path : '', component: ProjectsListComponent},
  { path : 'details/:id', component: ProjectDetailsComponent},
  { path : 'update/:id', component: ProjectUpdateComponent},
  { path : 'add', component: ProjectAddComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
