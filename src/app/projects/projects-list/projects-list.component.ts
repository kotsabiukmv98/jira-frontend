import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Project } from 'src/app/shared/models/project';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

  projects: Project[];
  constructor(private projectService: ProjectService){
  }

  ngOnInit() {
    this.refresh();
  }
  refresh(){
    this.projectService.getProjects().subscribe(projects =>{
      this.projects = projects;
    })
  }
  delete(projectId: number){
    this.projectService.deleteProject(projectId).subscribe(() =>{
      console.log(`Deleted project with ID ${projectId}`);
      this.refresh();
    })
  }
}
