import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NewProject } from 'src/app/shared/models/newProject';
import { ProjectService } from 'src/app/shared/services/project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent implements OnInit {

  projectForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    deadline: new FormControl(),
    authorId: new FormControl(),
    teamId: new FormControl()
  });
  constructor(
    private projectService: ProjectService,
    private router: Router) { }

  ngOnInit() {
  }
  save(){
    const newProject: NewProject = {
      name: this.projectForm.value.name,
      description: this.projectForm.value.description,
      deadline: this.projectForm.value.deadline,
      authorId: this.projectForm.value.authorId,
      teamId: this.projectForm.value.teamId
    };
    this.projectService.addProject(newProject).subscribe(() => {
        this.router.navigateByUrl('/projects'); 
      });
  }
}
