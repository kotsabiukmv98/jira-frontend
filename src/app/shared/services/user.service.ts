import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { NewUser } from '../models/newUser';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl = 'https://mylittlejirabackend.azurewebsites.net/api/users';
  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>(this.apiUrl);
  }

  getUserById(userId: number): Observable<User>{
    return this.http.get<User>(`${this.apiUrl}/${userId}`);
  }

  addUser(newUser: NewUser): Observable<User>{
    return this.http.post<User>(this.apiUrl, newUser);
  }

  updateUser(userId: number, updatedUser: NewUser): Observable<User>{    
    return this.http.put<User>(`${this.apiUrl}/${userId}`, updatedUser);
  }

  deleteUser(userId: number): Observable<any>{
    return this.http.delete(`${this.apiUrl}/${userId}`);
  }
}
