import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from '../models/project';
import { NewProject } from '../models/newProject';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  apiUrl = 'https://mylittlejirabackend.azurewebsites.net/api/projects';

  constructor(private http: HttpClient) {
  }

  getProjects(): Observable<Project[]>{
    return this.http.get<Project[]>(this.apiUrl);
  }

  getProjectById(projectId: number): Observable<Project>{
    return this.http.get<Project>(`${this.apiUrl}/${projectId}`);
  }

  addProject(newProject: NewProject): Observable<Project>{
    return this.http.post<Project>(this.apiUrl, newProject);
  }

  updateProject(projectId: number, updatedProject: NewProject): Observable<Project>{
    return this.http.put<Project>(`${this.apiUrl}/${projectId}`, updatedProject);
  }

  deleteProject(projectId: number): Observable<any>{
    return this.http.delete(`${this.apiUrl}/${projectId}`);
  }
}
