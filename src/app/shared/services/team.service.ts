import { Injectable } from '@angular/core';
import { Team } from '../models/team';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewTeam } from '../models/newTeam';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  apiUrl = 'https://mylittlejirabackend.azurewebsites.net/api/teams';
  constructor(private http: HttpClient) {
   }

  getTeams(): Observable<Team[]>{
    return this.http.get<Team[]>(this.apiUrl);
  }

  getTeamById(teamId: number): Observable<Team>{
    return this.http.get<Team>(`${this.apiUrl}/${teamId}`);
  }

  addTeam(newTeam: NewTeam): Observable<Team>{
    return this.http.post<Team>(this.apiUrl, newTeam);
  }

  updateTeam(teamId: number, newTeam: NewTeam): Observable<Team>{    
    return this.http.put<Team>(`${this.apiUrl}/${teamId}`, newTeam);
  }

  deleteTeam(teamId: number): Observable<any>{
    return this.http.delete(`${this.apiUrl}/${teamId}`);
  }
}
