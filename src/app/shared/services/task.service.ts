import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task';
import { NewTask } from '../models/newTask';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
  apiUrl = 'https://mylittlejirabackend.azurewebsites.net/api/tasks';
  constructor(private http: HttpClient) {
  }

  getTasks(): Observable<Task[]>{
    return this.http.get<Task[]>(this.apiUrl);
  }

  getTaskById(taskId: number): Observable<Task>{
    return this.http.get<Task>(`${this.apiUrl}/${taskId}`);
  }

  addTask(newTask: NewTask): Observable<Task>{
    return this.http.post<Task>(this.apiUrl, newTask);
  }

  updateTask(taskId: number, updatedTask: NewTask): Observable<Task>{    
    return this.http.put<Task>(`${this.apiUrl}/${taskId}`, updatedTask);
  }

  deleteTask(taskId: number): Observable<any>{
    return this.http.delete(`${this.apiUrl}/${taskId}`);
  }
}
