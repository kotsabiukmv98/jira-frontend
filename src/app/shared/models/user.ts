export class User
{
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    registeredAt: Date;
    teamId: number;
    birthday: Date;
}