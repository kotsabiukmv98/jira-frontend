export class NewUser{
    firstName: string
    lastName: string
    email: string
    teamId: number;
    birthday: Date;
}