export class NewTask{
    name: string
    description: string
    state: number;
    projectId: number;
    performerId: number;
}